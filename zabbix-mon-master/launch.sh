#!/bin/bash

if [ $# -ne 1 ]; then
  echo "Usage : $0 <Instance type> (zbx-db, zbx-db-replica, zbx-srv, zbx-web, zbx-prx)"
  exit -1
fi

ZBX_BASE=/opt/monitoring_deploy
ZBX_CFG=$ZBX_BASE/cfg
ZBX_LOGS=$ZBX_BASE/data/logs
ZBX_DB=/db/zabbix_db/data
mkdir -p $ZBX_LOGS

docker network create --driver bridge zbx.net
docker login -u docker-token -p yL7s6bZsDehR_vChN7HC gitlab.org:4567

case "$1" in
"zbx-db")
    echo "Starting: $1"
    mkdir -p $ZBX_DB
    sudo chown -R 70:70 $ZBX_DB
    touch $ZBX_LOGS/zbx-agt $ZBX_LOGS/zbx-db
    docker-compose -f dc_db.yml pull
    docker-compose -f dc_db.yml up -d --remove-orphans
    ;;
"zbx-db-replica")  echo  "Sending SIGINT signal"
    echo "Starting: $1"
    mkdir -p $ZBX_DB
    sudo chown -R 70:70 $ZBX_DB
    touch $ZBX_LOGS/zbx-agt $ZBX_LOGS/zbx-db-replica
    docker-compose -f dc_db-replica.yml pull
    docker-compose -f dc_db-replica.yml up -d --remove-orphans
    ;;
"zbx-srv")  echo  "Sending SIGQUIT signal"
    echo "Starting: $1"
    touch $ZBX_LOGS/zbx-agt $ZBX_LOGS/zbx-srv
    docker-compose -f dc_srv.yml pull
    docker-compose -f dc_srv.yml up -d --remove-orphans
  ;;
"zbx-web") echo  "Sending SIGKILL signal"
    echo "Starting: $1"
    touch $ZBX_LOGS/zbx-agt $ZBX_LOGS/zbx-web
    docker-compose -f dc_web.yml pull
    docker-compose -f dc_web.yml up -d --remove-orphans
   ;;
"zbx-prx") echo  "Sending SIGKILL signal"
    echo "Starting: $1"
    touch $ZBX_LOGS/zbx-agt $ZBX_LOGS/zbx-prx
    docker-compose -f dc_prx.yml pull
    docker-compose -f dc_prx.yml up -d --remove-orphans
   ;;

*) echo "Unknown instance name: $1"
   exit -1
   ;;
esac

