#!/bin/sh

stop() {
 echo "> SIGTERM signal received, try to gracefully shutdown all services..."
 echo "> Stopping: nginx"
 kill $(cat /var/run/nginx.pid)
 echo "> Stopping: php-fpm"
 kill $(cat /var/run/php-fpm.pid)
}

trap "stop; exit 0" SIGTERM SIGINT


# frontend config
sed -i "s/^\(\$DB\['TYPE'\]\).*/\1 = 'POSTGRESQL';/" /usr/share/webapps/zabbix/conf/zabbix.conf.php
sed -i "s/^\(\$DB\['SERVER'\]\).*/\1 = '$PGSQL_HOST';/" /usr/share/webapps/zabbix/conf/zabbix.conf.php
sed -i "s/^\(\$DB\['PORT'\]\).*/\1 = '$PGSQL_PORT';/" /usr/share/webapps/zabbix/conf/zabbix.conf.php
sed -i "s/^\(\$DB\['DATABASE'\]\).*/\1 = '$PGSQL_DB';/" /usr/share/webapps/zabbix/conf/zabbix.conf.php
sed -i "s/^\(\$DB\['USER'\]\).*/\1 = '$PGSQL_USER';/" /usr/share/webapps/zabbix/conf/zabbix.conf.php
sed -i "s/^\(\$DB\['PASSWORD'\]\).*/\1 = '$PGSQL_PASS';/" /usr/share/webapps/zabbix/conf/zabbix.conf.php

sed -i "s/^\(\$ZBX_SERVER\)\s.*/\1 = '$ZBX_SRV_HOST';/" /usr/share/webapps/zabbix/conf/zabbix.conf.php
sed -i "s/^\(\$ZBX_SERVER_PORT\)\s.*/\1 = '$ZBX_SRV_PORT';/" /usr/share/webapps/zabbix/conf/zabbix.conf.php
sed -i "s/^\(\$ZBX_SERVER_NAME\)\s.*/\1 = '$ZBX_SRV_NAME';/" /usr/share/webapps/zabbix/conf/zabbix.conf.php

# Disable ES Integration
sed -i "s/^\(\$HISTORY\['types'\].*\)/#\1/" /usr/share/webapps/zabbix/conf/zabbix.conf.php

# Set/Update Docker Host IP
HIP=`/sbin/ip route|awk '/default/ { print $3 }'`
if grep "host-ip" /etc/hosts; then
  cp -f /etc/hosts /tmp/hosts.new
  sed -i "s/.*host-ip.*/$HIP host-ip/g" /tmp/hosts.new
  cat /tmp/hosts.new > /etc/hosts
else
  echo "$HIP host-ip">> /etc/hosts
fi

# Disable ssl host if cert or key is not found to prevent nginx failing
if [[ ! -f /etc/nginx/ssl/chain.pem || ! -f /etc/nginx/ssl/cert.key ]]; then
  echo "/etc/nginx/ssl/chain.pem or /etc/nginx/ssl/cert.key, disabling /etc/nginx/hosts.d/default-ssl.conf"
  mv /etc/nginx/hosts.d/default-ssl.conf /etc/nginx/hosts.d/default-ssl.conf.disabled
fi

# Enable redirect http if REDIRECT_HTTP is true
if [[ $REDIRECT_HTTP == true ]]; then
  # disable default.conf
  mv /etc/nginx/hosts.d/default.conf /etc/nginx/hosts.d/default.conf.disabled
  # enable http-to-https redirect
  mv /etc/nginx/hosts.d/http-to-https.conf.disabled /etc/nginx/hosts.d/http-to-https.conf
fi


echo "> Starting: rsyslog"
rm -f /var/run/rsyslogd.pid
rsyslogd -f /etc/rsyslog.conf

echo "> Starting: php-fpm"
/usr/bin/php-fpm5

echo "> Starting: nginx"
/usr/sbin/nginx

while true; do
 sleep 15s & wait $!;
 ps -o pid |egrep "^\s+?$(cat /var/run/nginx.pid)\$" 1>/dev/null || exit 1;
 ps -o pid |egrep "^\s+?$(cat /var/run/php-fpm.pid)\$" 1>/dev/null || exit 1;
done

