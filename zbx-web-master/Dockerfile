# Usage:
#
# Standalone:
# docker build --force-rm -t zabbix-web .
# docker run -d --name zbx-web -h zbx-web -p 80:80 zbx-web
#

#
FROM        alpine:latest
MAINTAINER  Elena Gromova <elena.gromova717@gmail.com>

ARG         APK_FLAGS_COMMON="-q"
ARG         APK_FLAGS_PERSISTANT="${APK_FLAGS_COMMON} --clean-protected --no-cache"
ARG         APK_FLAGS_DEV="${APK_FLAGS_COMMON} --no-cache"
ARG         DB_TYPE=postgresql
ARG         ZBX_SOURCES="https://sourceforge.net/projects/zabbix/files/ZABBIX%20Latest%20Stable/4.0.4/zabbix-4.0.4.tar.gz/download"

ENV         LANG=en_US.UTF-8 \
            TERM=xterm \
            ZBX_SOURCES=${ZBX_SOURCES} \
            ZBX_SRV_HOST=localhost \
            ZBX_SRV_PORT=10051 \
            ZBX_SRV_NAME= \
            DB_TYPE=${DB_TYPE} \
            PGSQL_HOST=localhost \
            PGSQL_PORT=5432 \
            PGSQL_DB=zabbix \
            PGSQL_USER=zabbix \
            PGSQL_PASS=zabbix \
            REDIRECT_HTTP=false

RUN         apk update && apk upgrade \
            && apk add ${APK_FLAGS_PERSISTANT} rsyslog wget postgresql-client nginx php5-fpm php5-sockets php5-gd php5-gettext php5-bcmath php5-ctype php5-xmlreader php5-json php5-ldap php5-pgsql ttf-dejavu pcre \
            && apk add ${APK_FLAGS_DEV} --virtual build-deps tzdata bash curl tar coreutils gettext pcre-dev wget \
            && rm -f /etc/localtime && ln -s /usr/share/zoneinfo/UTC /etc/localtime \
            && mkdir -p /tmp/zabbix /var/spool/rsyslog \
            && wget --tries=4 --retry-connrefused --timeout=2 --wait=2 --waitretry=2 --no-verbose --no-check-certificate -O - "${ZBX_SOURCES}" | tar -xz -C /tmp/zabbix --strip-components 1 \
            && mkdir -p /usr/share/webapps/ \
            && cp -rp /tmp/zabbix/frontends/php /usr/share/webapps/zabbix \
            && /usr/share/webapps/zabbix/locale/make_mo.sh \
            && rm /usr/share/webapps/zabbix/fonts/DejaVuSans.ttf \
            && ln -s /usr/share/fonts/ttf-dejavu/DejaVuSans.ttf /usr/share/webapps/zabbix/fonts/DejaVuSans.ttf \
            && DATE=`date +%Y-%m-%d` \
            && sed -e "s/ZABBIX_VERSION.*'\(.*\)'/ZABBIX_VERSION', '\1 ($DATE)'/g" \
                   -e "s/ZABBIX_API_VERSION.*'\(.*\)'/ZABBIX_API_VERSION', '\1 ($DATE)'/g" -i /usr/share/webapps/zabbix/include/defines.inc.php \
            && sed -e "s/;\(pid =\).*/\1 \/var\/run\/php-fpm.pid/g" \
                   -e "s/;\(log_level =\).*/\1 warning/g" \
                   -e "s/^error_log.*/error_log = syslog/g" -i /etc/php5/php-fpm.conf \
            && sed -e "s/;error_log = syslog/error_log = syslog/g" -i /etc/php5/php.ini \
            && cp /usr/share/webapps/zabbix/conf/zabbix.conf.php.example /usr/share/webapps/zabbix/conf/zabbix.conf.php \
            && apk del ${APK_FLAGS_COMMON} --purge build-deps \
            && rm -rf /var/cache/apk/* /tmp/*

ADD         rootfs/ /
RUN         chmod 755 /*.sh

EXPOSE      80/TCP
CMD         ["/run.sh"]

HEALTHCHECK --interval=15s --timeout=3s --retries=3 CMD wget -U Docker-HealthCheck -Y off --no-check-certificate -O /dev/null http://localhost:80 || exit 1
ENV         https_proxy='' http_proxy='' HTTPS_PROXY='' HTTP_PROXY=''

