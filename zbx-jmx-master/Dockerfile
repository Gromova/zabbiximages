# Usage:
#
# docker build --force-rm -t zbx-jmx .
# docker run -d --name zbx-jmx -h zbx-jmx -p 10052:10052 zbx-jmx
#
# Gitlab:
# 
#
FROM        alpine:latest
MAINTAINER  Elena GROMOVA <ELENA.GROMOVA717@gmail.com>

ARG         APK_FLAGS_COMMON="-q"
ARG         APK_FLAGS_PERSISTANT="${APK_FLAGS_COMMON} --clean-protected --no-cache"
ARG         APK_FLAGS_DEV="${APK_FLAGS_COMMON} --no-cache"
ARG         DB_TYPE=postgresql
ARG         ZBX_SOURCES="https://sourceforge.net/projects/zabbix/files/ZABBIX%20Latest%20Stable/4.0.4/zabbix-4.0.4.tar.gz/download"
ARG         ZBX_JMX_START_POLLERS=5
ARG         ZBX_JMX_TIMEOUT=3
ARG         ZBX_JMX_DEBUGLEVEL=info

ENV         LANG=en_US.UTF-8 \
            TERM=xterm \
            ZBX_SOURCES=${ZBX_SOURCES} \
            ZBX_START_POLLERS=${ZBX_JMX_START_POLLERS} \
            ZBX_TIMEOUT=${ZBX_JMX_TIMEOUT} \
            ZBX_DEBUGLEVEL=${ZBX_JMX_DEBUGLEVEL}

RUN         echo '@edge http://nl.alpinelinux.org/alpine/edge/main' >>/etc/apk/repositories \
            && apk update && apk upgrade \
            && echo "http://dl-4.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories \
            && apk add ${APK_FLAGS_PERSISTANT} openjdk8-jre pcre \
            && mkdir -p /etc/zabbix/ \
            && apk add ${APK_FLAGS_DEV} --virtual build-deps tzdata alpine-sdk coreutils automake autoconf openjdk8 pcre-dev wget \
            && rm -f /etc/localtime && ln -s /usr/share/zoneinfo/UTC /etc/localtime \
            && cd /tmp/ \
            && mkdir -p /tmp/zabbix \
            && wget --tries=4 --retry-connrefused --timeout=2 --wait=2 --waitretry=2 --no-verbose --no-check-certificate -O - "${ZBX_SOURCES}" | tar -xz -C /tmp/zabbix --strip-components 1 \
            && cd /tmp/zabbix \
            && export PATH=/usr/lib/jvm/java-1.8-openjdk/bin:$PATH \
            && ./configure --silent --prefix=/usr --sysconfdir=/etc/zabbix --libdir=/usr/lib/zabbix --enable-java \
            && make -j"$(nproc)" -s 1>/dev/null \
            && mkdir -p /usr/sbin/zabbix_jmx/ \
            && cp -r src/zabbix_java/bin /usr/sbin/zabbix_jmx/ \
            && cp -r src/zabbix_java/lib /usr/sbin/zabbix_jmx/ \
            && apk del ${APK_FLAGS_COMMON} --purge build-deps \
            && rm -rf /var/cache/apk/* /tmp/zabbix/

ADD         rootfs/ /
RUN         chmod 755 /*.sh

EXPOSE      10052/TCP
CMD         ["/run.sh"]

HEALTHCHECK --interval=15s --timeout=3s --retries=3 CMD netstat -an |grep LISTEN |grep 0052 || exit 1
ENV         https_proxy='' http_proxy='' HTTPS_PROXY='' HTTP_PROXY=''
