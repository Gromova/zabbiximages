#!/bin/sh

JAVA=${JAVA:-"/usr/bin/java"}
DAEMON=${DAEMON:-"/usr/sbin/zabbix_jmx"}
CONFIG="/etc/zabbix/logback.xml"
JAVA_OPTIONS="-server $JAVA_OPTIONS"
JAVA_OPTIONS="$JAVA_OPTIONS -Dlogback.configurationFile=$CONFIG"

cd $DAEMON
CLASSPATH="$DAEMON/lib"
for jar in `find lib bin -name "*.jar"`; do
    if [ $jar != *junit* ]; then
        CLASSPATH="$CLASSPATH:$DAEMON/$jar"
    fi
done

if [ -n "${ZBX_DEBUGLEVEL}" ]; then
    echo "> Updating $CONFIG 'DebugLevel' parameter: '${ZBX_DEBUGLEVEL}'... updated"
    if [ -f "$CONFIG" ]; then
        sed -i -e "/^.*<root level=/s/=.*/=\"${ZBX_DEBUGLEVEL}\">/" "$CONFIG"
    else
        echo "> Zabbix Java Gateway log configuration file '$CONFIG' not found"
        exit
    fi
fi

ZABBIX_OPTIONS=""
if [ -n "$ZBX_START_POLLERS" ]; then
    ZABBIX_OPTIONS="$ZABBIX_OPTIONS -Dzabbix.startPollers=$ZBX_START_POLLERS"
fi
if [ -n "$ZBX_TIMEOUT" ]; then
    ZABBIX_OPTIONS="$ZABBIX_OPTIONS -Dzabbix.timeout=$ZBX_TIMEOUT -Dsun.rmi.transport.tcp.responseTimeout=${ZBX_TIMEOUT}000"
fi

COMMAND_LINE="$JAVA $JAVA_OPTIONS -classpath $CLASSPATH $ZABBIX_OPTIONS com.zabbix.gateway.JavaGateway"


# Set/Update Docker Host IP
HIP=`/sbin/ip route|awk '/default/ { print $3 }'`
if grep "host-ip" /etc/hosts; then
  cp -f /etc/hosts /tmp/hosts.new
  sed -i "s/.*host-ip.*/$HIP host-ip/g" /tmp/hosts.new
  cat /tmp/hosts.new > /etc/hosts
else
  echo "$HIP host-ip">> /etc/hosts
fi


echo "> Starting: Zabbix JMX Gateway"
exec $COMMAND_LINE
