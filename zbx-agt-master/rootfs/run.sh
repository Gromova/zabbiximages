#!/bin/sh

# Skip file config if its a volume
if !($( df |grep "zabbix_agentd.conf$" >/dev/null )); then
  sed -e "s/.*\(LogType=\).*/\1system/g" \
      -e "s/^\(Server=\).*/\1$ZBX_SRV_HOST/g" \
      -e "s/^\(ServerActive=\).*/\1$ZBX_SRV_HOST_ACT/g" \
      -e "s/.*\(StartAgents=\).*/\1$ZBX_AGT_NUM/g" \
      -e "s/.*\(ListenPort=\).*/\1$ZBX_AGT_PORT/g" \
      -e "s/^# AllowRoot=0/AllowRoot=1/g" \
      -e "s/^\(Hostname=\).*/\1`hostname -s`/g" -i /etc/zabbix/zabbix_agentd.conf
fi

# Set/Update Docker Host IP
HIP=`/sbin/ip route|awk '/default/ { print $3 }'`
if grep "host-ip" /etc/hosts; then
  cp -f /etc/hosts /tmp/hosts.new
  sed -i "s/.*host-ip.*/$HIP host-ip/g" /tmp/hosts.new
  cat /tmp/hosts.new > /etc/hosts
else
  echo "$HIP host-ip">> /etc/hosts
fi

echo "> Starting: rsyslog"
rm -f /var/run/rsyslogd.pid
rsyslogd -f /etc/rsyslog.conf

echo "> Starting: Agent"
chown -R $ZBX_AGT_USR /etc/zabbix/ /var/run/zabbix/ /var/log/zabbix/
exec sudo -u $ZBX_AGT_USR /usr/sbin/zabbix_agentd -f -c /etc/zabbix/zabbix_agentd.conf

