CREATE TEMP TABLE temp_events AS (SELECT eventid FROM events);

DELETE FROM event_tag WHERE eventid NOT IN (SELECT * FROM temp_events);
DELETE FROM event_recovery WHERE eventid NOT IN (SELECT * FROM temp_events);
DELETE FROM event_recovery WHERE r_eventid NOT IN (SELECT * FROM temp_events);
DELETE FROM event_recovery WHERE c_eventid NOT IN (SELECT * FROM temp_events);
DELETE FROM event_suppress WHERE eventid NOT IN (SELECT * FROM temp_events);
DELETE FROM problem WHERE eventid NOT IN (SELECT * FROM temp_events);
DELETE FROM problem WHERE r_eventid NOT IN (SELECT * FROM temp_events);
DELETE FROM alerts WHERE eventid NOT IN (SELECT * FROM temp_events);
DELETE FROM acknowledges WHERE eventid NOT IN (SELECT * FROM temp_events);
