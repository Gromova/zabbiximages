-- Disable AutoVacuum on partitioned tables
ALTER TABLE events SET (autovacuum_enabled = false, toast.autovacuum_enabled = false);
ALTER TABLE alerts SET (autovacuum_enabled = false, toast.autovacuum_enabled = false);
ALTER TABLE history SET (autovacuum_enabled = false, toast.autovacuum_enabled = false);
ALTER TABLE history_uint SET (autovacuum_enabled = false, toast.autovacuum_enabled = false);
ALTER TABLE history_str SET (autovacuum_enabled = false, toast.autovacuum_enabled = false);
ALTER TABLE history_text SET (autovacuum_enabled = false, toast.autovacuum_enabled = false);
ALTER TABLE history_log SET (autovacuum_enabled = false, toast.autovacuum_enabled = false);
ALTER TABLE trends SET (autovacuum_enabled = false, toast.autovacuum_enabled = false);
ALTER TABLE trends_uint SET (autovacuum_enabled = false, toast.autovacuum_enabled = false);

-- Drop Constrains on the events table
ALTER TABLE ONLY alerts DROP CONSTRAINT c_alerts_2;
ALTER TABLE ONLY alerts DROP CONSTRAINT c_alerts_5;
ALTER TABLE ONLY acknowledges DROP CONSTRAINT c_acknowledges_2;
ALTER TABLE ONLY event_tag DROP CONSTRAINT c_event_tag_1;
ALTER TABLE ONLY problem DROP CONSTRAINT c_problem_1;
ALTER TABLE ONLY problem DROP CONSTRAINT c_problem_2;
ALTER TABLE ONLY event_recovery DROP CONSTRAINT c_event_recovery_1;
ALTER TABLE ONLY event_recovery DROP CONSTRAINT c_event_recovery_2;
ALTER TABLE ONLY event_recovery DROP CONSTRAINT c_event_recovery_3;
ALTER TABLE ONLY event_suppress DROP CONSTRAINT c_event_suppress_1;
ALTER TABLE ONLY events DROP CONSTRAINT events_pkey;
ALTER TABLE ONLY alerts DROP CONSTRAINT alerts_pkey;

-- Stats
CREATE EXTENSION IF NOT EXISTS pg_stat_statements;
CREATE EXTENSION IF NOT EXISTS pg_buffercache;

-- Enable HyperTable
CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;
-- Week Chunk
SELECT create_hypertable('events', 'clock', chunk_time_interval => 604800);
SELECT create_hypertable('alerts', 'clock', chunk_time_interval => 604800);
SELECT create_hypertable('history', 'clock', chunk_time_interval => 604800);
SELECT create_hypertable('history_log', 'clock', chunk_time_interval => 604800);
SELECT create_hypertable('history_str', 'clock', chunk_time_interval => 604800);
SELECT create_hypertable('history_text', 'clock', chunk_time_interval => 604800);
SELECT create_hypertable('history_uint', 'clock', chunk_time_interval => 604800);
-- Month Chunk
SELECT create_hypertable('trends', 'clock', chunk_time_interval => 2592000);
SELECT create_hypertable('trends_uint', 'clock', chunk_time_interval => 2592000);

