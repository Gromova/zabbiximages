#!/bin/sh
# cron schedule
#0 0 * * * /usr/share/zabbix/database/postgresql/hypertable/chunk_cleaner.sh

ZBX_PURGE_RAW=${ZBX_PURGE_RAW:="8"}
ZBX_PURGE_TREND=${ZBX_PURGE_TREND:="24"}

date +"%d-%m-%y %H:%M:%S - start table purging: ZBX_PURGE_RAW=${ZBX_PURGE_RAW} / ZBX_PURGE_TREND=${ZBX_PURGE_TREND}" >> /var/lib/postgresql/data/$DB_VER/log/chunk_cleaner.log 2>&1
echo "chunk_cleaner: start table purging: ZBX_PURGE_RAW=${ZBX_PURGE_RAW} / ZBX_PURGE_TREND=${ZBX_PURGE_TREND}" | logger -t postgres -p notice

psql -d zabbix -U zabbix -c "SELECT drop_chunks(CAST(extract('epoch' from now() - interval '${ZBX_PURGE_RAW} months') AS int), 'events');" >> /var/lib/postgresql/data/$DB_VER/log/chunk_cleaner.log 2>&1
echo "chunk_cleaner: drop_chunks: events / rc=$?" | logger -t postgres -p notice
psql -d zabbix -U zabbix -c "SELECT drop_chunks(CAST(extract('epoch' from now() - interval '${ZBX_PURGE_RAW} months') AS int), 'alerts');" >> /var/lib/postgresql/data/$DB_VER/log/chunk_cleaner.log 2>&1
echo "chunk_cleaner: drop_chunks: alerts / rc=$?" | logger -t postgres -p notice
psql -d zabbix -U zabbix -c "SELECT drop_chunks(CAST(extract('epoch' from now() - interval '${ZBX_PURGE_RAW} months') AS int), 'history');" >> /var/lib/postgresql/data/$DB_VER/log/chunk_cleaner.log 2>&1
echo "chunk_cleaner: drop_chunks: history / rc=$?" | logger -t postgres -p notice
psql -d zabbix -U zabbix -c "SELECT drop_chunks(CAST(extract('epoch' from now() - interval '${ZBX_PURGE_RAW} months') AS int), 'history_log');" >> /var/lib/postgresql/data/$DB_VER/log/chunk_cleaner.log 2>&1
echo "chunk_cleaner: drop_chunks: history_log / rc=$?" | logger -t postgres -p notice
psql -d zabbix -U zabbix -c "SELECT drop_chunks(CAST(extract('epoch' from now() - interval '${ZBX_PURGE_RAW} months') AS int), 'history_str');" >> /var/lib/postgresql/data/$DB_VER/log/chunk_cleaner.log 2>&1
echo "chunk_cleaner: drop_chunks: history_str / rc=$?" | logger -t postgres -p notice
psql -d zabbix -U zabbix -c "SELECT drop_chunks(CAST(extract('epoch' from now() - interval '${ZBX_PURGE_RAW} months') AS int), 'history_text');" >> /var/lib/postgresql/data/$DB_VER/log/chunk_cleaner.log 2>&1
echo "chunk_cleaner: drop_chunks: history_text / rc=$?" | logger -t postgres -p notice
psql -d zabbix -U zabbix -c "SELECT drop_chunks(CAST(extract('epoch' from now() - interval '${ZBX_PURGE_RAW} months') AS int), 'history_uint');" >> /var/lib/postgresql/data/$DB_VER/log/chunk_cleaner.log 2>&1
echo "chunk_cleaner: drop_chunks: history_uint / rc=$?" | logger -t postgres -p notice

psql -d zabbix -U zabbix -c "SELECT drop_chunks(CAST(extract('epoch' from now() - interval '${ZBX_PURGE_TREND} months') AS int), 'trends');" >> /var/lib/postgresql/data/$DB_VER/log/chunk_cleaner.log 2>&1
echo "chunk_cleaner: drop_chunks: trends / rc=$?" | logger -t postgres -p notice
psql -d zabbix -U zabbix -c "SELECT drop_chunks(CAST(extract('epoch' from now() - interval '${ZBX_PURGE_TREND} months') AS int), 'trends_uint');" >> /var/lib/postgresql/data/$DB_VER/log/chunk_cleaner.log 2>&1
echo "chunk_cleaner: drop_chunks: trends_uint / rc=$?" | logger -t postgres -p notice


# Remove orphan event records
echo "chunk_cleaner: remove orphan event records" | logger -t postgres -p notice
psql -d zabbix -U zabbix < /usr/share/zabbix/database/postgresql/hypertable/clean_orphans.sql >> /var/lib/postgresql/data/$DB_VER/log/chunk_cleaner.log 2>&1
echo "chunk_cleaner: finished" | logger -t postgres -p notice

