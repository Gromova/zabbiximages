#!/bin/sh
# cron schedule
#0 0 1 * * /usr/share/zabbix/database/postgresql/hypertable/backups.sh

TS=$( date '+%Y%m%d' )

mkdir -p /var/lib/postgresql/data/$DB_VER/pg_backup/
date +"%d-%m-%y %H:%M:%S" >> /var/lib/postgresql/data/$DB_VER/log/backup.log 2>&1
pg_dump -U postgres --blobs --dbname zabbix -T public.event* -T public.alerts* -T public.problem* -T public.acknowledges* -T public.history* -T public.trends* -T _timescaledb_internal.* -T _timescaledb_catalog.* --format=c --compress=4 --file="/var/lib/postgresql/data/$DB_VER/pg_backup/$TS.sql.gz" >> /var/lib/postgresql/data/$DB_VER/log/backup.log 2>&1
echo "backups: /var/lib/postgresql/data/$DB_VER/pg_backup/$TS.sql.gz / rc: $?" | logger -t postgres -p notice

