#!/bin/bash
set -e

ZBX_INITDATA=${ZBX_INITDATA:="local"}
ZBX_INITADM=${ZBX_INITADM:=""}
PGSQL_DB=${PGSQL_DB:="zabbix"}
PGSQL_USER=${PGSQL_USER:="zabbix"}
PGSQL_PASS=${PGSQL_PASS:-}
PG_DATDIR="/var/lib/postgresql/data/${DB_VER}"
PG_BINDIR="/usr/local/bin"


create_database() {

    if [ -z "$(ls -A "$PG_DATDIR/PG_VERSION" 2>/dev/null)" ]; then
    echo "> Init postgres"
    mkdir -p $PG_DATDIR/
    chmod 0750 $PG_DATDIR/ || :
    chown -R postgres $PG_DATDIR/ || :
    sudo -u postgres $PG_BINDIR/initdb -D $PG_DATDIR

    if [ -f "/root/postgresql.conf" ]; then
      cp /root/postgresql.conf $PG_DATDIR/
      chown postgres:postgres $PG_DATDIR/postgresql.conf || :
      chmod 0600 $PG_DATDIR/postgresql.conf || :
    else
      sed -e "s/^#?(listen_addresses\s*=\s*)\S+/\1'*'/" \
          -e "s/^#?(logging_collector\s*=\s*)\S+/\1on/" -ri "$PG_DATDIR"/postgresql.conf
    fi

    if [ -f "/root/pg_hba.conf" ]; then
      cp /root/pg_hba.conf $PG_DATDIR/
      chown postgres:postgres $PG_DATDIR/pg_hba.conf || :
      chmod 0600 $PG_DATDIR/pg_hba.conf || :
    else
      { echo -e "\nhost all all 0.0.0.0/0 md5"; } >> "$PG_DATDIR"/pg_hba.conf
    fi

    echo "> Starting postgres"
    sudo -u postgres $PG_BINDIR/pg_ctl -w start -D $PG_DATDIR

  else
    echo "> Starting postgres (DB Check/Update)"
    sudo -u postgres $PG_BINDIR/pg_ctl -w start -D $PG_DATDIR

    DB_INSTALLED=`sudo -u postgres psql -l | grep -c ${PGSQL_DB}`
    if [ $DB_INSTALLED -ne 0 ]; then
      echo "> Zabbix Database (${PGSQL_DB}) already exists"
      sudo -u postgres psql -X -c "\c $PGSQL_DB;" -c "ALTER EXTENSION timescaledb UPDATE;"
      sudo -u postgres psql -X $PGSQL_DB -c "\dx;"

      ZBX_VER=(`psql -t -d $PGSQL_DB -U $PGSQL_USER -c "SELECT mandatory FROM dbversion;"`)
      if [ ${ZBX_VER[0]} -lt 4000000 ]; then
        echo "> ZBX Version: ${ZBX_VER[0]} (Upgraded Needed)"
        sudo -u postgres psql -U ${PGSQL_USER} -d ${PGSQL_DB} -f /usr/share/zabbix/database/postgresql/upgrade_300_400.sql >>/root/upgrade_database.log 2>&1
      else
        echo "> ZBX Version: ${ZBX_VER[0]}"
      fi

      echo "> Stopping postgres"
      sudo -u postgres $PG_BINDIR/pg_ctl stop -D $PG_DATDIR
      exit 0
    fi
  fi

  echo "> Zabbix Database creation"

  # Check to see if we have pre-defined credentials to use
  if [ -n "${PGSQL_USER}" ]; then
    if [ -z "${PGSQL_PASS}" ]; then
      echo "> WARNING: No password specified for \"${PGSQL_USER}\". Generating one"
      PGSQL_PASS=$(pwgen -c -n -1 12)
      echo "> Password for \"${PGSQL_USER}\" created as: \"${PGSQL_PASS}\""
    fi
    echo "> Creating Role/User: ${PGSQL_USER} with Password: ${PGSQL_PASS}"
    sudo -u postgres psql -c "CREATE ROLE ${PGSQL_USER} WITH CREATEROLE SUPERUSER LOGIN PASSWORD '${PGSQL_PASS}';"
    sudo -u postgres psql -c "CREATE ROLE zabbix_ro WITH NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION LOGIN PASSWORD 'Wg88kSNtJ3Hav7jrm2U2io3MRVS5Hn';"
  fi

  if [ -n "${PGSQL_DB}" ]; then
    echo "> Creating database \"${PGSQL_DB}\"..."
    sudo -u postgres psql -c "CREATE DATABASE ${PGSQL_DB} WITH OWNER=${PGSQL_USER} ENCODING='UTF8' lc_collate='en_US.UTF-8' lc_ctype='en_US.UTF-8';"
  fi
  if [ -n "${PGSQL_USER}" ]; then
    echo "> Granting access to database \"${PGSQL_DB}\" for user \"${PGSQL_USER}\"..."
    sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE ${PGSQL_DB} to ${PGSQL_USER};"
  fi

  echo "> Populating the Database: ${PGSQL_DB}"
  sudo -u postgres psql -U ${PGSQL_USER} -d ${PGSQL_DB} -f /usr/share/zabbix/database/postgresql/schema.sql >>/root/create_database.log 2>&1
  sudo -u postgres psql -U ${PGSQL_USER} -d ${PGSQL_DB} -f /usr/share/zabbix/database/postgresql/images.sql >>/root/create_database.log 2>&1
  if [ -f "/usr/share/zabbix/database/postgresql/data_custom_$ZBX_INITDATA.sql" ]; then
    if [ $(echo "$ZBX_INITDATA" | tr A-Z a-z) == "local" ]; then
      echo "> Zabbix DB Initialization: ZBX_INITDATA = $ZBX_INITDATA"
      sudo -u postgres psql -U ${PGSQL_USER} -d ${PGSQL_DB} -f /usr/share/zabbix/database/postgresql/data_custom_$ZBX_INITDATA.sql >>/root/create_database.log 2>&1
    else
      if [ "$ZBX_INITADM" != "" ]; then
        echo "> Zabbix DB Initialization: ZBX_INITDATA = $ZBX_INITDATA / ZBX_INITADM = $ZBX_INITADM"
        sed -i "s/<ZBX_INITADM>/$(echo "$ZBX_INITADM" | tr A-Z a-z)/g" /usr/share/zabbix/database/postgresql/data_custom_$ZBX_INITDATA.sql 2>&1
        sudo -u postgres psql -U ${PGSQL_USER} -d ${PGSQL_DB} -f /usr/share/zabbix/database/postgresql/data_custom_$ZBX_INITDATA.sql >>/root/create_database.log 2>&1
      else
        echo "> Zabbix DB Initialization data was not found: ZBX_INITADM = $ZBX_INITADM"
        exit 1
      fi
    fi
  else
    echo "> Zabbix DB Initialization data was not found: ZBX_INITDATA = $ZBX_INITDATA"
    exit 1
  fi

  echo "> Activating table partitioning on Database: ${PGSQL_DB}"
  sudo -u postgres psql -U ${PGSQL_USER} -d ${PGSQL_DB} -f /usr/share/zabbix/database/postgresql/hypertable/01_create_hypertable.sql >>/root/create_database.log

  if [ -n "${PGSQL_USER}" ]; then
    echo "> Granting access to database \"${PGSQL_DB}\" for user \"zabbix_ro\"..."
    sudo -u postgres psql $PGSQL_DB -c "GRANT CONNECT ON DATABASE ${PGSQL_DB} to zabbix_ro;"
    sudo -u postgres psql $PGSQL_DB -c "GRANT USAGE ON SCHEMA public TO zabbix_ro;"
    sudo -u postgres psql $PGSQL_DB -c "GRANT SELECT ON ALL TABLES IN SCHEMA public TO zabbix_ro;"
  fi


  if ! [[ -z $PGSQL_REPLICA_NAME ]]; then
    echo "> Activating Replication"

    if [[ -z $PGSQL_REPLICATE_FROM ]]; then
      echo "> Config Primary"
      sudo -u postgres psql -U ${PGSQL_USER} -c "SET password_encryption = 'scram-sha-256'; CREATE ROLE $PGSQL_REPLICA_POSTGRES_USER WITH REPLICATION PASSWORD '$PGSQL_REPLICA_POSTGRES_PASSWORD' LOGIN;"

      # Add replication settings to primary postgres conf
      sed -e "s/^#\(wal_level =.*\)/\1/g" -i "$PG_DATDIR"/postgresql.conf
      sed -e "s/^#\(max_wal_senders =.*\)/\1/g" -i "$PG_DATDIR"/postgresql.conf
      sed -e "s/^#\(max_replication_slots =.*\)/\1/g" -i "$PG_DATDIR"/postgresql.conf

      if ! [[ -z $PGSQL_SYNCHRONOUS_COMMIT ]]; then
        sed -e "s/^\(synchronous_commit =\).*/\1 $PGSQL_SYNCHRONOUS_COMMIT/g" -i "$PG_DATDIR"/postgresql.conf
      fi

      # Add synchronous standby names if we're in one of the synchronous commit modes
      if [[ "${PGSQL_SYNCHRONOUS_COMMIT}" =~ ^(on|remote_write|remote_apply)$ ]]; then
        sed -e "s/^#\(synchronous_standby_names =.*\)/\1 '1 \($PGSQL_REPLICA_NAME\)'/g" -i "$PG_DATDIR"/postgresql.conf
      fi

      # Add replication settings to primary pg_hba.conf
      # Using the hostname of the primary doesn't work with docker containers, so we resolve to an IP using getent or we use a subnet provided at runtime.
      if  [[ -z $PGSQL_REPLICATION_SUBNET ]]; then
        PGSQL_REPLICATION_SUBNET=$(getent hosts ${PGSQL_REPLICATE_TO} | awk '{ print $1 }')/32
      fi
      echo -e "host    replication     ${PGSQL_REPLICA_POSTGRES_USER}         ${PGSQL_REPLICATION_SUBNET}           scram-sha-256" >> ${PG_DATDIR}/pg_hba.conf

      # Restart postgres and add replication slot
      sudo -u postgres $PG_BINDIR/pg_ctl -D ${PG_DATDIR} -m fast -w restart
      sudo -u postgres $PG_BINDIR/psql -U ${PGSQL_USER} -c "SELECT * FROM pg_create_physical_replication_slot('${PGSQL_REPLICA_NAME}_slot');"

    else
      echo "> Config Replica"

      # Stop postgres instance and clear out PGDATA
      sudo -u postgres $PG_BINDIR/pg_ctl -D ${PG_DATDIR} -m fast -w stop
      rm -rf ${PG_DATDIR}

      # Create a pg pass file so pg_basebackup can send a password to the primary
      echo -e "*:5432:replication:${PGSQL_REPLICA_POSTGRES_USER}:${PGSQL_REPLICA_POSTGRES_PASSWORD}" > ~/.pgpass.conf
      chown postgres:postgres ~/.pgpass.conf || :
      chmod 0600 ~/.pgpass.conf || :

      # Backup replica from the primary
      until PGPASSFILE=~/.pgpass.conf $PG_BINDIR/pg_basebackup -h ${PGSQL_REPLICATE_FROM} -D ${PG_DATDIR} -U ${PGSQL_REPLICA_POSTGRES_USER} -vP -w
      do
          # If docker is starting the containers simultaneously, the backup may encounter
          # the primary amidst a restart. Retry until we can make contact.
          sleep 1
          echo "> Retrying backup . . ."
      done

      # Remove pg pass file -- it is not needed after backup is restored
      rm ~/.pgpass.conf

      # Create the recovery.conf file so the backup knows to start in recovery mode
      echo "standby_mode = on" >>${PG_DATDIR}/recovery.conf
      echo "primary_conninfo = 'host=${PGSQL_REPLICATE_FROM} port=5432 user=${PGSQL_REPLICA_POSTGRES_USER} password=${PGSQL_REPLICA_POSTGRES_PASSWORD} application_name=${PGSQL_REPLICA_NAME}'" >>${PG_DATDIR}/recovery.conf
      echo "primary_slot_name = '${PGSQL_REPLICA_NAME}_slot'" >>${PG_DATDIR}/recovery.conf

      # Ensure proper permissions on recovery.conf
      chown -R postgres:postgres ${PG_DATDIR}/ || :
      chmod 0600 ${PG_DATDIR}/recovery.conf || :

      sudo -u postgres $PG_BINDIR/pg_ctl -D ${PG_DATDIR} -w start
    fi
  fi

  echo "> Stopping postgres"
  sudo -u postgres $PG_BINDIR/pg_ctl stop -D $PG_DATDIR

  echo "> Init finished, details: /root/create_database.log"
}

start_database() {
  echo "> Activating table partitioning auto cleaning"
  echo "0 0 * * * /usr/share/zabbix/database/postgresql/hypertable/chunk_cleaner.sh" >/etc/crontabs/postgres
  echo "0 0 1 * * /usr/share/zabbix/database/postgresql/hypertable/backup.sh" >>/etc/crontabs/postgres
  echo "> Starting crond"
  crond

  echo "> Starting postgres"
  chmod 0750 $PG_DATDIR/ || :
  chown -R postgres:postgres /var/lib/postgresql || :
  sudo -u postgres $PG_BINDIR/postgres -D $PG_DATDIR &
  wait
}

stop_database() {
  echo "> Stopping postgres : SIGTERM signal received, try to gracefully shutdown all services..."
  sudo -u postgres $PG_BINDIR/pg_ctl stop -m fast -D $PG_DATDIR
}


trap "stop_database; exit 0" SIGTERM SIGINT


# Set/Update Docker Host IP
HIP=`/sbin/ip route|awk '/default/ { print $3 }'`
if grep "host-ip" /etc/hosts; then
  cp -f /etc/hosts /tmp/hosts.new
  sed -i "s/.*host-ip.*/$HIP host-ip/g" /tmp/hosts.new
  cat /tmp/hosts.new > /etc/hosts
else
  echo "$HIP host-ip">> /etc/hosts
fi

echo "> Starting: rsyslog"
rm -f /var/run/rsyslogd.pid
rsyslogd -f /etc/rsyslog.conf

create_database 2>&1 | tee -a /root/create_database.log
start_database

