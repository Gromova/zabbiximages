#!/bin/sh
set -e


# Skip file config if its a volume
if !($( df |grep "zabbix_proxy.conf$" >/dev/null )); then
  sed -e "s/.*\(LogType=\).*/\1system/g" \
      -e "s/.*\(ProxyMode=\).*/\1$ZBX_PXY_MODE/g" \
      -e "s/.*\(ProxyOfflineBuffer=\).*/\1$ZBX_PXY_OFFBUFFER/g" \
      -e "s/^\(Server=\).*/\1$ZBX_SRV_HOST/g" \
      -e "s/.*\(ServerPort=\).*/\1$ZBX_SRV_PORT/g" \
      -e "s/^\(Hostname=\).*/\1`hostname`/g" \
      -e "s/^\(DBName=\).*/\1$(echo "$SQLi_DB" | sed 's/[\/&]/\\&/g')/g" \
      -e "s/^\(DBUser=\).*/\1$SQLi_USER/g" -i /etc/zabbix/zabbix_proxy.conf
fi

# Set/Update Docker Host IP
HIP=`/sbin/ip route|awk '/default/ { print $3 }'`
if grep "host-ip" /etc/hosts; then
  cp -f /etc/hosts /tmp/hosts.new
  sed -i "s/.*host-ip.*/$HIP host-ip/g" /tmp/hosts.new
  cat /tmp/hosts.new > /etc/hosts
else
  echo "$HIP host-ip">> /etc/hosts
fi

echo "> Starting: rsyslog"
rm -f /var/run/rsyslogd.pid
rsyslogd -f /etc/rsyslog.conf

echo "> Starting: Proxy"
mkdir -p /var/lib/sqlite/ /var/log/zabbix/
chown -R zabbix:zabbix /var/lib/sqlite/ /var/log/zabbix/
exec sudo -u zabbix /usr/sbin/zabbix_proxy -f -c /etc/zabbix/zabbix_proxy.conf
