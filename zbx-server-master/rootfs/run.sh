#!/bin/sh

# Skip file config if its a volume
if !($( df |grep "zabbix_server.conf$" >/dev/null )); then
  sed -e "s/.*\(LogType=\).*/\1system/g" \
      -e "s/.*\(ListenPort=\).*/\1$ZBX_SRV_PORT/g" \
      -e "s/.*\(DBHost=\).*/\1$PGSQL_HOST/g" \
      -e "s/^\(DBName=\).*/\1$PGSQL_DB/g" \
      -e "s/.*\(DBPort=\).*/\1$PGSQL_PORT/g" \
      -e "s/^\(DBUser=\).*/\1$PGSQL_USER/g" \
      -e "s/.*\(DBPassword=\).*/\1$PGSQL_PASS/g" -i /etc/zabbix/zabbix_server.conf
fi

# Set/Update Docker Host IP
HIP=`/sbin/ip route|awk '/default/ { print $3 }'`
if grep "host-ip" /etc/hosts; then
  cp -f /etc/hosts /tmp/hosts.new
  sed -i "s/.*host-ip.*/$HIP host-ip/g" /tmp/hosts.new
  cat /tmp/hosts.new > /etc/hosts
else
  echo "$HIP host-ip">> /etc/hosts
fi

echo "> Starting: rsyslog"
rm -f /var/run/rsyslogd.pid
rsyslogd -f /etc/rsyslog.conf

echo "> Starting: Server"
exec sudo -u zabbix /usr/sbin/zabbix_server -f -c /etc/zabbix/zabbix_server.conf
